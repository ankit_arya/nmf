loc_data= fullfile(pwd,'/Datasets/Caltech101/trainTest/TrainingData.mat'); 
load(loc_data);
training_labels=training_labels-2;
train_label_names=train_label_names(3:end);
dataset='Caltech101'

%acc=cell(1,length(unique(labels)));

tr_indx=30*(1:100)+1;
tr_indx=[1 tr_indx];
% set up for call to nmf_train;
parfor i= 1:length(unique(training_labels)),
	
	neg_indx=find(training_labels~=i); % contains negative labels
	
	ix=randperm(length(neg_indx));%contain random entries
	pos_indx=find(training_labels==i);  %contain positive labels
	
	neg_data= training_Data(:,ix(1:1000));	
	pos_data= training_Data(:,tr_indx(i):tr_indx(i)+29);
	
	tr_Data= [pos_data,neg_data]; %TRAINING DATA
	tr_labels=[ones(30,1);-1*ones(1000,1)];

	nmf_train(tr_Data,tr_labels,256,dataset,train_label_names{i})


end


%tr_count=tr_count+30; %add 30 so to keep track of training data

	%load 'Training.mat'
	%score=sign((G'*G)*beta +bias);
	%acc=length(find(score==tr_labels))/length(tr_labels);
	%fprintf('acc on training data:%d',acc);
	%name=fullfile(pwd,sprintf('/caltech_data/classifer_class%s',train_label_names{i}));
	%save(name,'G','beta','bias','tr_labels','score','tr_Data');


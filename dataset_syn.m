% Generate synthetic dataset described in Dasgupta et al

datadim = 7;
F = rand(3,datadim)';

n1=200;
mu1=[10 10 10];
sigma1=[1 0 0;0 1 0; 0 0 1];
X1=mvnrnd(mu1,sigma1,n1);

n2=200;
mu2=[50 50 50];
sigma2=[1 0 0; 0 1 0; 0 0 1];
X2=mvnrnd(mu2,sigma2,n2);

G=[X1;X2]';

%scatter(G(:,1),G(:,2),G(:,3),'DisplayName','G(:,2) vs. G(:,1)','YDataSource','G(:,2)');figure(gcf)
scatter3(G(1,:),G(2,:),G(3,:));
Y=sign([-ones(n1,1,'double');ones(n2,1,'double')]);


Data = F * G + wgn(datadim,n1+n2,0) + 1.0;

pt = linspace(30,230,200)';
Xtest = [pt pt pt pt pt pt pt];
numTest = size(Xtest,1);

%for i = 1:numTest
%    disp(Xtest(i,:));
%end

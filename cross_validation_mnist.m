%function cross_validation_mnist(K, numTrain)
    K = 4;
    numTrain = 10;


    r = 32; %reduced dimensionallity
    numIter = 100;
    %numTrain = 100;
    numTrainPos = numTrain;
    numTrainNeg = numTrain;

    %addpath('../datasets/mnist/');
    addpath('MNIST_solver/')
    addpath('MNIST_solver/mnistHelper/')

    imagesInp = loadMNISTImages('train-images.idx3-ubyte');
    labelsInp = loadMNISTLabels('train-labels.idx1-ubyte');
    numClasses = 10;

    numData = length(imagesInp);
    rndPerm = randperm(numData);
    images  = imagesInp(:,rndPerm);
    labels  = labelsInp(rndPerm);

    Y = cell(10,1);
    L = cell(10,1);

    for digit = 0:9
        Y_pos{digit+1} = find(labels == digit);
        L_pos{digit+1} = length(Y_pos{digit+1});
        Y_neg{digit+1} = find(labels ~= digit);
        L_neg{digit+1} = length(Y_neg{digit+1});
    end
 
N = numTrain;

for k = 1:K
    vl{k} = k:K:N;   
end

for k = 1:K
    tr{k} = setdiff(1:N, vl{k});
end

Acc = cell(K,1);

for k = 1:K 
    for digit = 0:9
        fprintf('Training for digit %d',digit);
        idnum = str2num(getenv('SGE_TASK_ID'));
        % if idnum == digit
        pos = images(:,Y_pos{digit+1});
        neg = images(:,Y_neg{digit+1});
        %Data = [ pos(:,:), neg(:,:)];
        %Data = [ pos(:,1:numTrainPos), neg(:,1:numTrainNeg)];
        Data = [ pos(:,tr{k}), neg(:,tr{k})];
        trPos = size(pos,2);
        trNeg = size(neg,2);
        %Y = [ ones(trPos,1) ;ones(trNeg,1) ];
        numTrPos = length(tr{k}); 
        numTrNeg = length(tr{k}); 
        Y = [ ones(numTrPos,1) ; -1 * ones(numTrNeg,1) ];
        nmf_train(Data,Y,r,'mnist',sprintf('%d',digit));
        % end
    end
    numIter = 100;

    loc_data=fullfile(pwd,'/Datasets/');
    dataset='mnist';
    run_test(loc_data,dataset,images(:,vl{k}),numClasses,numIter);
    Acc{k} = gen_results(loc_data,dataset,(labels(vl{k})+1)');

end

avg = 0.0;
fprintf('\nThe accuracies are :');
for k = 1:K
    fprintf('%g ', 100.0 * Acc{k});
    avg = avg + Acc{k};
end
avg = avg / K;
fprintf('\n');
fprintf('The average cross validation accuracy is %g per cent\n', 100.0 * avg);


%end

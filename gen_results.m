function [Acc] = gen_results(loc_data, dataset, test_labels)

load(fullfile(loc_data,dataset,'/trainTest/Classifier_score.mat'));
[predict]=winnerTakesAll(Classifier_score);
[CM,order]=confusionmat(test_labels,predict');

pngfile = fullfile(loc_data,dataset,'/results/confusion.png');
f = figure;
hold off;
h=HeatMap(CM,'RowLabels', 0:9, 'ColumnLabels', 0:9);
titlestr = sprintf('Confusion Matrix for %s', dataset);
addTitle(h,titlestr);
addXLabel(h);
addYLabel(h);
plot(h,f);
saveas(f,pngfile);

Acc=length(find((test_labels)==predict))/length(test_labels);
fprintf('Accuracy on testing data is : %g\%',Acc*100);

end



for digit =0:9
	fprintf('Training for digit %d',digit);
    idnum = str2num( getenv('SGE_TASK_ID') );
   % if idnum == digit
        pos = images(:,Y_pos{digit+1});
        neg = images(:,Y_neg{digit+1});
        %Data = [ pos(:,:), neg(:,:)];
        Data = [ pos(:,1:numTrainPos), neg(:,1:numTrainNeg)];
        trPos = size(pos,2);
        trNeg = size(neg,2);
        %Y = [ ones(trPos,1) ;ones(trNeg,1) ];
        Y = [ ones(numTrainPos,1) ; -1 * ones(numTrainNeg,1) ];
        nmf_train(Data,Y,r,'mnist',sprintf('%d',digit));
    %end
end

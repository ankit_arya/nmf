function nmf_train(Data,Y,r,dataset,class_name)
global X K
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize random W and H
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[m,n]=size(Data);  %n training vectors
F=rand(m,r);
G=rand(r,n);  %This will be the input for the SVM
initial_gamma = 10000   ;
gamma = initial_gamma;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SVM stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X=G;
K = X'*X;  %kernel matrix
lambda = 1;
opt.iter_max_Newton=1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Newton Method Optimization%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
grad_G=zeros(r,n); A=cell(n,1);
maxiter=100;
cost=inf; p_cost=0; 
close all;

for iter =1:maxiter,
	%%%%%UPADATES%%%%%%%%%%
	%if norm(cost-p_cost)<=1,
    %    break;
    %end
	%%%F update%%%%%%%
	F = F.*(G*Data')'./(F*(G*G')+eps);
	
	
	%t1=tic;
	%%Beta Updata%%%%%%%%
	[beta,bias,sv,obj,out]=primal_svm(0,Y,lambda,opt);
	loss=out;
	loss(find(out<=0))=0;
	%fprintf('time for primal svm update %g',toc(t1));
	
	h1=figure(1);
	plot(iter,obj,'r*','linewidth', 4);drawnow; hold on;
	title(sprintf('SVM Plot for digit %s',class_name)) ; 
	xlabel('No. of iterations'); ylabel('SVM primal Obj val');

	%% G Update%%%%%%%%%%
	%beta is 10x1, G is 2x10, F is 3x2, X is 3x10
	%calculate gradient
    %t2=tic;	
	
	grad_G=2*gamma*(-F'*Data + (F'*F)*G)+( 2*lambda*beta*( sum(G .* repmat(beta,1,r)', 2) )')'+...
        repmat(2*( sum(G(:,sv).*repmat((loss(sv).*beta(sv)),1,r)',2)),1,n) +...   
        (sum(G(:,sv).*repmat(loss(sv),1,r)',2))*beta';
  
	for i=1:n,
		%gradient of G
		%{
		grad_G(:,i)= 2*gamma*(-F'*Data(:,i) + (F'*F)*G(:,i))+ ...
		 2*lambda*beta(i)*( sum(G .* repmat(beta,1,r)', 2) ) + ...
		 2*( sum(G(:,sv).*repmat((loss(sv).*beta(sv)),1,r)',2)) +...
		  beta(i)*(sum(G(:,sv).*repmat(loss(sv),1,r)',2));
		%}
	 	%Auxillary function
		A{i}=2*gamma*(F'*F) + (2*( lambda*beta(i)^2+ ...
		 (beta(i)^2+loss(i)^2)*any(i==sv)))*eye(r);
		 %update for G
		G(:,i)=G(:,i).*((A{i}*G(:,i)-grad_G(:,i))./(A{i}*G(:,i)));
	end
	%fprintf('time for G update %g',toc(t2));
	%COST Function
    p_cost=cost;
	cost= norm(Data-F*G)+ 2*lambda*beta'*K*beta+ sum(loss(sv))^2;
	
    % Update
    gamma = initial_gamma / (1.0 + eps)^iter;
    
    %figure
    h2=figure(2);
	plot(iter,cost, 'b*', 'linewidth', 4); 	drawnow ;hold on;
	title(sprintf('Objective Func Minimization for digit %s',class_name)) ; 
	xlabel('No. of iterations'); ylabel('Objective Func val');
	
	%UPDATE THE KERNEL
	X=G;
	K=X'*X;    
end

validate(Data,F,G,Y,beta,bias,class_name,dataset);
name_fig1= fullfile(pwd,sprintf('/Datasets/%s/results/Digit%s/SVMobj_%s.png',dataset,class_name,maxiter));
name_fig2= fullfile(pwd,sprintf('/Datasets/%s/results/Digit%s/MAINobj_%s.png',dataset,class_name,maxiter));
saveas(h1,name_fig1);
saveas(h2,name_fig2);
name=fullfile(pwd,sprintf('/Datasets/%s/classifiers/model_%s.mat',dataset,class_name));
save(name,'F','G','loss','sv','gamma','lambda','Data','beta','r','bias');

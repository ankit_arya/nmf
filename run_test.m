function run_test(location,dataset,test_Data,numClasses,numIter)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% location= Location of the Datasets folder
% dataset = name of the dataset you want to run
% test_data= Testing data mat file

%@Ankit Arya
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

beta=0;

frames = dir(fullfile(location,dataset,'/classifiers/*.mat'));
c_num = length(frames); 
Classifier_score=zeros(size(test_Data,2),numClasses);
maxIter=numIter; %(max iterations for NMF test)
G_test=cell(1,c_num);
k = cell(c_num,1);
v = cell(c_num,1);
for ii = 1:c_num,  
    	%load(fullfile(location,dataset,'/classifiers/',frames(ii).name));
        modelfile = sprintf('model_%d.mat',ii-1);
    	load(fullfile(location,dataset,'/classifiers/',modelfile));
		fprintf('Processing data for %s \n',frames(ii).name); 
		Beta=beta;
		[Gtest] = nmf_classify(test_Data,F,size(F,2),maxIter);	% Calls NMF
		[raw_scores]= computeScore(Gtest,G,Beta,bias);			% Computes raw score
		Classifier_score(:,ii)=raw_scores;						% Updates Classifer Score Matrix
        G_test{ii}=Gtest;
end

name=fullfile(location,dataset,'/trainTest/Classifier_score.mat');
save(name,'Classifier_score','G_test');

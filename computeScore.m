function [raw_scores]= computerScore(Gtest,G,Beta,bias),
	raw_scores = (Gtest' * G* Beta + bias);
end	

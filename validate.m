function validate(Data,F,G,Y,beta,bias,class_name,dataset)

    numSamples = size(Data,2);
    
	%Orignal Data
	for i=1:numSamples
		I1(:,:,1,i)=reshape(Data(:,i),[28 28]);
	end
	
	%Reconstructed Data
	for i=1:numSamples
		I2(:,:,1,i)=reshape(F*G(:,i),[28 28]);
	end

	h1=figure;
	set(gcf,'numbertitle','off','name','Original data') 
	montage(I1)
	
	h2=figure;
	set(gcf,'numbertitle','off','name','Re-constructed data') 
	montage(I2)
	
	% check acc on training data
	[raw_scores]= computeScore(G,G,beta,bias);			% Computes raw score
	predict=sign(raw_scores);						% Updates Classifer Score Matrix
	acc_tr=length(find(Y==predict))/length(Y);
	fprintf('Acc on training data %g\% \n',acc_tr*100);

    name_folder = fullfile(pwd,sprintf('/Datasets/%s/results/Digit%s',dataset,class_name));
    mkdir(name_folder);
	name_fig1= fullfile(pwd,sprintf('/Datasets/%s/results/Digit%s/origData.png',dataset,class_name));
	name_fig2= fullfile(pwd,sprintf('/Datasets/%s/results/Digit%s/reconsData.png',dataset,class_name));
	saveas(h1,name_fig1);
	saveas(h2,name_fig2);
	
	
	
end

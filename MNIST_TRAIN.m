
images = loadMNISTImages('train-images.idx3-ubyte');
labels = loadMNISTLabels('train-labels.idx1-ubyte');

numClasses = 10;
numTraining=1000;
r=32;

X=images(:,1:numTraining);
labels=labels(1:numTraining);
labels(find(labels==0))=10;

tr_labels=-1*ones(numTraining,1);
for digit=1:10
	tr_labels=-1*ones(length(labels),1);
	tr_labels(find(labels==digit))=1;
	nmf_train(X,tr_labels,r,'mnist',sprintf('%d',digit));
end
  
%function  [w,obj] = primal_svm_linear(Y,lambda,opt) 
function [F,G,w,obj]=nmf_svm(Data,K,maxiter,speak,Y,lambda,opt)
% INPUT:
% Data (N,M) : N (dimensionallity) x M (samples) non negative input matrix
% K       : Number of components
% maxiter : Maximum number of iterations to run
% speak   : prints iteration count and changes in connectivity matrix 
%           elements unless speak is 0
% Y		  : Training labels for SVM
% lambda  :LAMBDA is the regularization parameter ( = 1/C)
% OPT     :is a structure containing the options (in brackets default values):
%   		cg: Do not use Newton, but nonlinear conjugate gradients [0]
%   		lin_cg: Compute the Newton step with linear CG 
%           [0 unless solving sparse linear SVM]
%   		iter_max_Newton: Maximum number of Newton steps [20]
%   		prec: Stopping criterion
%   		cg_prec and cg_it: stopping criteria for the linear CG.
%
% OUTPUT:
% W       : N x K matrix
% H       : K x M matrix
% w		  : weights for SVM
% obj1	  : objective function value
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User adjustable parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

print_iter = 50; % iterations between print on screen and convergence test

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% test for negative values in X
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if min(min(X)) < 0
    error('Input matrix elements can not be negative');
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize random W and H
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[m,n]=size(Data);  %n training vectors
F=rand(m,K);
G=rand(K,n);  %This will be the input for the SVM

% use W*H to test for convergence
Datar_old=F*G;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize stuff for SVM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X=G;
[n,d] = size(X);  
w = zeros(d+1,1); % The last component of w is b.
iter = 0;
out = ones(n,1); % Vector containing 1-Y.*(X*w)
global K;
opt.iter_max_Newton=1;


While 1,
	iter = iter + 1;
	K=G.*G;
	[beta b]=primal_svm(0,Y,lambda,opt);
	
	%%error
	
	%%%
	
	
	
   % Euclidean multiplicative method
    F = F.*(G*F')'./(F*(G*G')+eps);    for i=1:size(G,2),
    	grad_G(:,i) = 2*gamma*(-F'*Data(:,i) + (F'F)*G(:,i)  + 2*lambda*beta(i)*(beta'*G')+ 2* 
    
    G = 
	X=G;  %assign the updated features as input to SVM
    
end

 

global X K
% Generating 3000 points in 100 dimensions
%X = randn(3000,100);
n1=100;
mu1=[50 50 50];
sigma1=[1 0 0;0 1 0 ; 0 0 1];
X1=mvnrnd(mu1,sigma1,n1);
n2=100;
mu2=[100 100 100];
sigma2=[1 0 0; 0 1 0; 0 0 1];
X2=mvnrnd(mu2,sigma2,n2);

X=[X1;X2];

scatter(X(:,1),X(:,2),'DisplayName','X(:,2) vs. X(:,1)','YDataSource','X(:,2)');figure(gcf)
Y=sign([-ones(n1,1,'double');ones(n2,1,'double')]);

%X = [Y X]; 
%Y = sign(X(:,1));
%X=X(:,2:end)
K = X*X';
lambda = 1;
[beta,b,sv]=primal_svm(0,Y,lambda);
%[w,b ]=primal_svm(1,Y,lambda);
%{ 
x=linspace(-20,120,100);
y=-b/w(2) +(-w(1)/w(2))*x;
hold on;
plot(x,y)
%}
sprintf('done')


% Test
xtestcoord = linspace(-300,300,600);
ytestcoord = linspace(-300,300,600);


Xtest = [xtestcoord' ytestcoord'];
scores = sign(Xtest * X'*beta +b);









%{

% The solutions are the same because the kernel is linear !
norm( K*beta+b - (X*w+b0)) 
% Try to now solve by conjugate gradient
opt.cg = 1;
[w,   b0 ]=primal_svm(1,Y,lambda,opt); 
[beta,b]=primal_svm(0,Y,lambda,opt);

norm( K*beta+b - (X*w+b0))

% Sparse linear problem
X = sprandn(1e5,1e4,1e-3);
Y = sign(sum(X,2)+randn(1e5,1));
[w,b]=primal_svm(1,Y,lambda); 
%}

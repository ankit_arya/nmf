function make_dirs(dataset)
    mkdir('Datasets/');
    mkdir(sprintf('Datasets/%s/',dataset));
    mkdir(sprintf('Datasets/%s/classifiers/',dataset));
    mkdir(sprintf('Datasets/%s/results/',dataset));
    mkdir(sprintf('Datasets/%s/trainTest/',dataset));
end


function nmf_test_sample(DataTest,F,beta,sv,loss,r)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize random W and H
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n = 1;  % we carry out the process for a single test sample
Data = DataTest;
[m,n]=size(Data);  %n training vectors
G=rand(r,n);  %This will be the input for the SVM
initial_gamma = 10000;
gamma = initial_gamma;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SVM stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X=G;
K = X'*X;  %kernel matrix
lambda = 1;
opt.iter_max_Newton=1;
gamma=1;
figure(1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Newton Method Optimization%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
grad_G=zeros(r,n);
maxiter=100;
for iter =1:maxiter,

    %%%%%UPADATES%%%%%%%%%%
    for i=1:n,
        %gradient of G

        grad_G(:,i)= 2*gamma*(-F'*Data(:,i) + (F'*F)*G(:,i))+ ...
        2*lambda*beta(i)*( sum(G .* repmat(beta,1,r)', 2) ) + ...
        2*( sum(G(:,sv).*repmat((loss(sv).*beta(sv)),1,r)',2)) +...
        beta(i)*(sum(G(:,sv).*repmat(loss(sv),1,r)',2));

        %Auxillary function
        A{i}=2*gamma*(F'*F) + (2*( lambda*beta(i)^2+ (beta(i)^2+loss(i)^2)*any(i==sv)))*eye(r);
        G(:,i)=G(:,i).*((A{i}*G(:,i)-grad_G(:,i))./(A{i}*G(:,i)));
    end

    %COST Function
    cost= norm(Data-F*G)+ 2*lambda*beta'*K*beta+ sum(loss);

    % Update
    gamma = initial_gamma / (1.0 + eps)^iter;

    plot(iter,log(cost), 'b*', 'linewidth', 4);
    X=G;
    hold on;
end

end	

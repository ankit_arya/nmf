loc_data= fullfile('/home/ankit/Courses/101_ObjectCategories');
subfolders=dir(loc_data);
%parfor_progress(length(subfolders));

training_Data=[]; training_labels=[]; train_label_names={};test_Data=[]; test_labels=[];
test_label_names={};

for ii = 1:length(subfolders),  
   %parfor_progress;
   subname = subfolders(ii).name;  
   if ~strcmp(subname, '.') && ~strcmp(subname, '..'),
		frames = dir(fullfile(loc_data, subname,'*.jpg')); 	
		c_num = length(frames);  
		ix = randperm(c_num);
		frames=frames(ix);  %shuffle the elements in frames
		fprintf('Processing data for %s \n',subname); 
		for i=1:30,
			%read the jpg image
			I= imread(fullfile(loc_data, subname,frames(i).name)); 
			if size(I,3)>1;
				I=rgb2gray(I);
			end	
			%Generate samples
			I=imresize(I,[128 128]);
			hog= vl_hog(im2single(I),8,'variant', 'dalaltriggs');
			training_Data=[training_Data,hog(:)];
			training_labels=[training_labels,ii];
		end
		lim=min(c_num,40);
		for i=31:lim,
			I= imread(fullfile(loc_data, subname,frames(i).name)); 
			if size(I,3)>1;
				I=rgb2gray(I);
			end	
			%Generate samples
			I=imresize(I,[128 128]);
			hog= vl_hog(im2single(I),8,'variant', 'dalaltriggs');
			test_Data=[test_Data,hog(:)];
			test_labels=[test_labels,ii];
		end
		train_label_names{ii}=subname;
		test_label_names{ii}=subname;
	end
	
end
loc=fullfile(pwd,'/caltech_data/TrainingData.mat');
save(loc,'training_Data','training_labels','train_label_names');

loc=fullfile(pwd,'/caltech_data/TestingData.mat');
save(loc,'test_Data','test_labels','test_label_names');

numIterTest=100;
testImages = loadMNISTImages('t10k-images.idx3-ubyte');
testLabels = loadMNISTLabels('t10k-labels.idx1-ubyte');
numClasses=10;

loc_data=fullfile(pwd,'/Datasets/');
dataset='mnist';
if do_test
run_test(loc_data,dataset,testImages,numClasses,numIterTest);
end

%testLabels(find(testLabels==0))=10;
gen_results(loc_data,dataset,(testLabels+1)');

function cost= nmf_cost(X,F,G)
	cost=norm(X-F*G);

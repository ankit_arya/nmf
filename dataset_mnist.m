%%%%%%%%%%%%%DATA%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

do_train = 1;
do_test = 1;
r = 32; %reduced dimensionallity
numIter = 100;
numTrain = 100;
numTrainPos = numTrain;
numTrainNeg = numTrain;

%addpath('../datasets/mnist/');
addpath('mnistHelper/')

images = loadMNISTImages('train-images.idx3-ubyte');
labels = loadMNISTLabels('train-labels.idx1-ubyte');
testImages = loadMNISTImages('t10k-images.idx3-ubyte');
testLabels = loadMNISTLabels('t10k-labels.idx1-ubyte');
numClasses = 10;


Y = cell(10,1);
L = cell(10,1);

for digit = 0:9
    Y_pos{digit+1} = find(labels == digit);
    L_pos{digit+1} = length(Y_pos{digit+1});
    Y_neg{digit+1} = find(labels ~= digit);
    L_neg{digit+1} = length(Y_neg{digit+1});

    Ytest_pos{digit+1} = find(testLabels == digit);
    Ltest_pos{digit+1} = length(Ytest_pos{digit+1});
    Ytest_neg{digit+1} = find(testLabels ~= digit);
    Ltest_neg{digit+1} = length(Ytest_neg{digit+1});
end

for digit = 0:9
    fprintf('Digit %d :\n\t Training: %d vs %d : %d\n\t Test: %d vs %d : %d\n', ...
        digit, ...
        L_pos{digit+1}, L_neg{digit+1}, ...
        L_pos{digit+1} + L_neg{digit+1}, ...
        Ltest_pos{digit+1}, Ltest_neg{digit+1}, ...
        Ltest_pos{digit+1} + Ltest_neg{digit+1} ...
        );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

loc_data= fullfile(pwd,'/Datasets/');
dataset='Caltech101'
test=load(fullfile(loc_data,dataset,'/trainTest/TestingData.mat'));

do_testing=false;
if do_testing,
	 %call to start testing
	run_test(loc_data,dataset,test.test_Data,length(unique(test.test_labels)),1000); 
end

test.test_labels=test.test_labels-2;
tlabels = test.test_labels;
gen_results(loc_data, dataset, tlabels);
